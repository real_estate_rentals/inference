import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
import mlflow
import pandas as pd
import os
from dotenv import load_dotenv

load_dotenv()
mlflow.set_experiment("sklearn")
mlflow.set_tracking_uri(os.environ['MLFLOW_TRACKING_URI'])
# os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://0.0.0.0:9000'

logged_model = os.environ['LOGGED_MODEL']
loaded_model = mlflow.pyfunc.load_model(logged_model)


class InputDataSchema(BaseModel):
    property_type: int
    bedrooms: int
    bathrooms: int
    lease_type: int
    promotion_type: int


app = FastAPI()


@app.post("/apartment")
async def create_person(input_data: InputDataSchema):
    predicted_price = loaded_model.predict(pd.DataFrame([input_data.dict()]))
    print(predicted_price)
    return {"message": f"Predicted price is {predicted_price[0]} USD"}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)
