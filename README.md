### Local setup

Copy file `.env_example` and fill values, rename to `.env'.

```
$ poetry install
$ poetry run python main.py 
```

### Example usage:

```
$ curl -X POST -H "Content-Type: application/json" -d '{"property_type": 1, "bedrooms": 2, "bathrooms": 1, "lease_type": 0, "promotion_type": 1}' http://localhost:8001/apartment

{"message":"Predicted price is 3476.4113757367686 USD"}%   
```
```
$ curl -X POST -H "Content-Type: application/json" -d '{"property_type": 1, "bedrooms": 20, "bathrooms": 1, "lease_type": 0, "promotion_type": 1}' http://localhost:8001/apartment

{"message":"Predicted price is 10821.907618306184 USD"}%   
```