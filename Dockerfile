FROM python:3.10

WORKDIR /code

RUN pip install poetry

COPY pyproject.toml poetry.lock /code/
RUN poetry install

ENV MLFLOW_S3_ENDPOINT_URL=http://host.docker.internal:9000
ENV MLFLOW_TRACKING_URI=http://host.docker.internal:5001

COPY main.py .env /code/

CMD ["poetry", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]